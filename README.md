# plot_tools
A collection of code snippets for plotting with matplotlib

# Requires:
* matplotlib

# Supplies:
The **plot_tools** module which exposes the following methods:
```python
    get_cm_colors(colormap, n_colors):
        # Obtain a number 'n_colors' of colors from a colormap
        # named 'colormap'.

```

# Installation:
Install with pip inside the root folder:
    pip install .
